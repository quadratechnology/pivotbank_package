﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using QERPUtill;
using DBUtil;
using ModuleUtil;
using PivotUtil.Data;
using DevExpress.Data.PivotGrid;
using System.Threading;
using System.Globalization;

namespace PivotBank
{
    public partial class FormBank : QEB.Form
    {
        #region Variable
        public string LoginString;
        public PopMessageLoading popMessageLoging;
        private DBSimple dbSimple;        
        private DataTable TBBankMaster = new DataTable();
        private PivotUtil.PivotCenter PivotCenterFunc = new PivotUtil.PivotCenter();
        private PivotFile PivotControlFileBank;
        private ModuleProgram ModuleProgramObj;
        #endregion

        public FormBank()
        {
            
            InitializeComponent();
        }

        private void FormBank_Load(object sender, EventArgs e)
        {
            this.QEBSetup(LoginString);
          
            InitControl();
            this.dbSimple = new DBSimple(QEBConnection);            

            
            popMessageLoging.CloseLoadingProgram();
        }

        #region Fucntion
        #region Init
        private void InitControl()
        {
            this.ModuleProgramObj = new ModuleProgram(this.QEBConnectionQEB, this.QEBCenterInfo.Company);
            this.Text = this.ModuleProgramObj.GetTitleProgram(LanguageClass.ModuleName,
                this.QEBCenterInfo.Server, this.QEBCenterInfo.Company, this.QEBCenterInfo.Username);

            PivotControlFileBank = new PivotFile(pivotGridBank,this.dbSimple,this.QEBCenterInfo.Company);

            this.WindowState = FormWindowState.Maximized;
        }
        
        #endregion

        private void toolMenuRefresh_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            string ErrMsg = string.Empty;
            int ErrCode = 0;
            int TransErr = 0;

            string sqlBank = @"select BM.AccountCode As AccountCodes
,BM.BankAccountNo As BankAccountNos
,BM.BankAccountType As BankAccountTypes
,BM.BankNameThai As BankNameThais
,BM.BankNameEng As BankNameEngs
,BM.BankBranchThai As BankBranchThais
,BM.BankBranchEng As BankBranchEngs
,BM.AddressThai As AddressThais
,BM.AddressEng As AddressEngs
,BM.SwiftCode As SwiftCodes
,BM.Code As Codes , BM.UpdatedUser
,AC.T As AccountNameT
,CU.T As CurrencyNameT
,BM.LastUpdated As LastUpdateds
,(CASE BM.RecStatus WHEN 0 THEN N'ปกติ'
                    WHEN 1 THEN N'ยกเลิก' END) As StatusName
from BankMaster BM
left join AccountChart AC
on AC.Code = BM.AccountCode
left join QERP.dbo.Currency CU
on CU.Code = BM.CurrencyCode";


            this.dbSimple.FillData(this.TBBankMaster, sqlBank, string.Empty, ref ErrCode, ref ErrMsg);
            TransErr += this.PivotCenterFunc.ErrMsgToMessagesShow(this.QEBCenterInfo, sqlBank, ref ErrMsg);
            if (TransErr != 0)
            {
                MessageBox.Show(ErrMsg, "การดึงข้อมูลผิดพลาด");
                this.Cursor = Cursors.Default;
                return;
            }

            if (this.TBBankMaster.Rows.Count == 0)
            {
                MessageBox.Show("ไม่พบข้อมูลธนาคาร");
                this.Cursor = Cursors.Default;
                return;
            }



            this.pivotGridBank.DataSource = this.TBBankMaster;
            this.pivotGridBank.RefreshData();
            this.Cursor = Cursors.Default;
        }
        private void toolMenuPreview_Click(object sender, EventArgs e)
        {
            if (!this.pivotGridBank.IsPrintingAvailable)
            {
                MessageBox.Show("The 'DevExpress.XtraPrinting.v7.2.dll' is not found", "Error");
                return;
            }

            this.pivotGridBank.ShowPrintPreview();
        }
        private void toolMenuLoad_Click(object sender, EventArgs e)
        {
            this.Cursor = Cursors.WaitCursor;
            string NameFileOut = "";
            if (OpenFile(this.openFileDialog1, "PivotBank.pvgrid", ref NameFileOut) == DialogResult.Cancel)
            {
                this.Cursor = Cursors.Default;
                return;
            }

            string PivotName = this.pivotGridBank.Name;
            string TabName = "ข้อมูลระบบบัญชี";
            string MsgShow = string.Empty;
            try
            {
                if (!this.PivotControlFileBank.HasTag(NameFileOut))
                {
                    PivotFileDataSource ds = new PivotFileDataSource(NameFileOut);
                    PivotCenterFunc.CheckTemplateFile(TabName, this.pivotGridBank, ds, ref MsgShow);
                    if (MsgShow != string.Empty)
                    {
                        MessageBox.Show(MsgShow);
                        this.Cursor = Cursors.Default;
                        return;
                    }
                }
                else
                {
                    PivotValue CheckTab = this.PivotControlFileBank.GetTag(NameFileOut);
                    if (CheckTab.Key == PivotName)
                    {
                        this.PivotControlFileBank.LoadFile(NameFileOut);
                    }
                    else
                    {
                        MessageBox.Show("ไม่สามารถเปิด File ได้ เนื่องจากชุดข้อมูลไม่ใช่ " + TabName + " กรุณาเปิด Tab " + CheckTab.TabName);
                        this.Cursor = Cursors.Default;
                        return;
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("ไม่สามารถเปิดไฟล์ได้");
            }
            this.Cursor = Cursors.Default;
        }
        private void toolMenuSave_Click(object sender, EventArgs e)
        {
            string PivotName = this.pivotGridBank.Name;
            string TabName = "วิเคราะห์ข้อมูลระบบบัญชี : ข้อมูลระบบบัญชี";
            string strPivot = PivotFile.CreateTag(PivotName, null, null, TabName);

            string NameFileOut = "";
            if (SaveFile(this.saveFileDialog1, "PivotBank.pvgrid", ref NameFileOut) == DialogResult.Cancel)
                return;
            this.PivotControlFileBank.SaveFile(NameFileOut, strPivot);
        }

        private DialogResult OpenFile(OpenFileDialog OpenDialog, string NameIn, ref string NameOut)
        {
            DialogResult result;
            OpenDialog.FileName = NameIn;
            OpenDialog.CheckFileExists = false;
            OpenDialog.Filter = "Pivot File (.pvgrid)|*.pvgrid";
            result = OpenDialog.ShowDialog();
            NameOut = OpenDialog.FileName;
            return (result);
        }
        private DialogResult SaveFile(SaveFileDialog SaveDialog, string NameIn, ref string NameOut)
        {
            DialogResult result;
            SaveDialog.FileName = NameIn;
            SaveDialog.CheckFileExists = false;
            SaveDialog.Filter = "Pivot File (.pvgrid)|*.pvgrid";
            result = SaveDialog.ShowDialog();
            NameOut = SaveDialog.FileName;
            return (result);
        }
        #endregion

    }
}
