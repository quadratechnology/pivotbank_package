﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Globalization;
using System.Threading;
using DBInfo;
using ModuleUtil;


namespace PivotBank
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            ControlCenterAddition.QERPCulture.SetCurrentThread(args);

            TBRequiredCheck[] tbRCheck = new TBRequiredCheck[] {
                new TBRequiredCheck("BankMaster",false,"10/10/2557")
            };
            string LoginInfo = ModuleLoad.LoadMainOK(args, tbRCheck, "BankMaster");
            if (LoginInfo != "")
            {
               
                FormBank fm = new FormBank();
                fm.LoginString = LoginInfo;
                fm.popMessageLoging = ModuleLoad.PopMessageLoadingObj;
                Application.Run(fm);
            }
            else Application.Exit();
        }
    }
}
