﻿namespace PivotBank
{
    partial class FormBank
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormBank));
            QEB.Center center1 = new QEB.Center();
            this.pivotGridBank = new ControlCenterAddition.XtraPivotGridControl();
            this.pivotFieldCode = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotFieldSwiftCode = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotFieldBankNameThai = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotFieldBankNameEng = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotFieldBankBranchThai = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotFieldBankBranchEng = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotFieldAddressThai = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotFieldAddressEng = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotFieldBankAccountType = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotFieldBankAccountNO = new DevExpress.XtraPivotGrid.PivotGridField();
            this.PivotFieldAccountNameT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotFieldAccountCodes = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotFieldCurrencyNameT = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotLastUpdateds = new DevExpress.XtraPivotGrid.PivotGridField();
            this.pivotFieldStatusName = new DevExpress.XtraPivotGrid.PivotGridField();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.layoutControl1 = new DevExpress.XtraLayout.LayoutControl();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.MenuFetchData = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuSave = new System.Windows.Forms.ToolStripMenuItem();
            this.MenuLoad = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPreview = new System.Windows.Forms.ToolStripMenuItem();
            this.layoutControlGroup1 = new DevExpress.XtraLayout.LayoutControlGroup();
            this.layoutPivot = new DevExpress.XtraLayout.LayoutControlItem();
            this.layoutMenuBank = new DevExpress.XtraLayout.LayoutControlItem();
            this.ColUpdatedUser = new DevExpress.XtraPivotGrid.PivotGridField();
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridBank)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).BeginInit();
            this.layoutControl1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPivot)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMenuBank)).BeginInit();
            this.SuspendLayout();
            // 
            // pivotGridBank
            // 
            this.pivotGridBank.AppearancePrint.Cell.BackColor = System.Drawing.Color.White;
            this.pivotGridBank.AppearancePrint.Cell.Options.UseBackColor = true;
            this.pivotGridBank.AppearancePrint.FieldHeader.BackColor = System.Drawing.Color.White;
            this.pivotGridBank.AppearancePrint.FieldHeader.Options.UseBackColor = true;
            this.pivotGridBank.AppearancePrint.FieldValue.BackColor = System.Drawing.Color.White;
            this.pivotGridBank.AppearancePrint.FieldValue.Options.UseBackColor = true;
            this.pivotGridBank.AppearancePrint.FieldValueGrandTotal.BackColor = System.Drawing.Color.White;
            this.pivotGridBank.AppearancePrint.FieldValueGrandTotal.Options.UseBackColor = true;
            this.pivotGridBank.Cursor = System.Windows.Forms.Cursors.Default;
            this.pivotGridBank.Fields.AddRange(new DevExpress.XtraPivotGrid.PivotGridField[] {
            this.pivotFieldCode,
            this.pivotFieldSwiftCode,
            this.pivotFieldBankNameThai,
            this.pivotFieldBankNameEng,
            this.pivotFieldBankBranchThai,
            this.pivotFieldBankBranchEng,
            this.pivotFieldAddressThai,
            this.pivotFieldAddressEng,
            this.pivotFieldBankAccountType,
            this.pivotFieldBankAccountNO,
            this.PivotFieldAccountNameT,
            this.pivotFieldAccountCodes,
            this.pivotFieldCurrencyNameT,
            this.pivotLastUpdateds,
            this.pivotFieldStatusName,
            this.ColUpdatedUser});
            resources.ApplyResources(this.pivotGridBank, "pivotGridBank");
            this.pivotGridBank.Name = "pivotGridBank";
            this.pivotGridBank.OptionsPrint.PrintDataHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.pivotGridBank.OptionsPrint.PrintFilterHeaders = DevExpress.Utils.DefaultBoolean.False;
            this.pivotGridBank.OptionsPrint.UsePrintAppearance = true;
            // 
            // pivotFieldCode
            // 
            this.pivotFieldCode.Appearance.Header.Options.UseTextOptions = true;
            this.pivotFieldCode.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotFieldCode.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotFieldCode.AreaIndex = 0;
            resources.ApplyResources(this.pivotFieldCode, "pivotFieldCode");
            this.pivotFieldCode.Name = "pivotFieldCode";
            this.pivotFieldCode.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldCode.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldCode.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // pivotFieldSwiftCode
            // 
            this.pivotFieldSwiftCode.Appearance.Header.Options.UseTextOptions = true;
            this.pivotFieldSwiftCode.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotFieldSwiftCode.AreaIndex = 0;
            resources.ApplyResources(this.pivotFieldSwiftCode, "pivotFieldSwiftCode");
            this.pivotFieldSwiftCode.Name = "pivotFieldSwiftCode";
            this.pivotFieldSwiftCode.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldSwiftCode.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldSwiftCode.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // pivotFieldBankNameThai
            // 
            this.pivotFieldBankNameThai.Appearance.Header.Options.UseTextOptions = true;
            this.pivotFieldBankNameThai.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotFieldBankNameThai.AreaIndex = 1;
            resources.ApplyResources(this.pivotFieldBankNameThai, "pivotFieldBankNameThai");
            this.pivotFieldBankNameThai.Name = "pivotFieldBankNameThai";
            this.pivotFieldBankNameThai.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldBankNameThai.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldBankNameThai.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // pivotFieldBankNameEng
            // 
            this.pivotFieldBankNameEng.Appearance.Header.Options.UseTextOptions = true;
            this.pivotFieldBankNameEng.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotFieldBankNameEng.AreaIndex = 2;
            resources.ApplyResources(this.pivotFieldBankNameEng, "pivotFieldBankNameEng");
            this.pivotFieldBankNameEng.Name = "pivotFieldBankNameEng";
            this.pivotFieldBankNameEng.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldBankNameEng.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldBankNameEng.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // pivotFieldBankBranchThai
            // 
            this.pivotFieldBankBranchThai.Appearance.Header.Options.UseTextOptions = true;
            this.pivotFieldBankBranchThai.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotFieldBankBranchThai.AreaIndex = 3;
            resources.ApplyResources(this.pivotFieldBankBranchThai, "pivotFieldBankBranchThai");
            this.pivotFieldBankBranchThai.Name = "pivotFieldBankBranchThai";
            this.pivotFieldBankBranchThai.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldBankBranchThai.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldBankBranchThai.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // pivotFieldBankBranchEng
            // 
            this.pivotFieldBankBranchEng.Appearance.Header.Options.UseTextOptions = true;
            this.pivotFieldBankBranchEng.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotFieldBankBranchEng.AreaIndex = 4;
            resources.ApplyResources(this.pivotFieldBankBranchEng, "pivotFieldBankBranchEng");
            this.pivotFieldBankBranchEng.Name = "pivotFieldBankBranchEng";
            this.pivotFieldBankBranchEng.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldBankBranchEng.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldBankBranchEng.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // pivotFieldAddressThai
            // 
            this.pivotFieldAddressThai.Appearance.Header.Options.UseTextOptions = true;
            this.pivotFieldAddressThai.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotFieldAddressThai.AreaIndex = 5;
            resources.ApplyResources(this.pivotFieldAddressThai, "pivotFieldAddressThai");
            this.pivotFieldAddressThai.Name = "pivotFieldAddressThai";
            this.pivotFieldAddressThai.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldAddressThai.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldAddressThai.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // pivotFieldAddressEng
            // 
            this.pivotFieldAddressEng.Appearance.Header.Options.UseTextOptions = true;
            this.pivotFieldAddressEng.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotFieldAddressEng.AreaIndex = 6;
            resources.ApplyResources(this.pivotFieldAddressEng, "pivotFieldAddressEng");
            this.pivotFieldAddressEng.Name = "pivotFieldAddressEng";
            this.pivotFieldAddressEng.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldAddressEng.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldAddressEng.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // pivotFieldBankAccountType
            // 
            this.pivotFieldBankAccountType.Appearance.Header.Options.UseTextOptions = true;
            this.pivotFieldBankAccountType.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotFieldBankAccountType.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotFieldBankAccountType.AreaIndex = 1;
            resources.ApplyResources(this.pivotFieldBankAccountType, "pivotFieldBankAccountType");
            this.pivotFieldBankAccountType.Name = "pivotFieldBankAccountType";
            this.pivotFieldBankAccountType.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldBankAccountType.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldBankAccountType.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // pivotFieldBankAccountNO
            // 
            this.pivotFieldBankAccountNO.Appearance.Header.Options.UseTextOptions = true;
            this.pivotFieldBankAccountNO.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotFieldBankAccountNO.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea;
            this.pivotFieldBankAccountNO.AreaIndex = 2;
            resources.ApplyResources(this.pivotFieldBankAccountNO, "pivotFieldBankAccountNO");
            this.pivotFieldBankAccountNO.Name = "pivotFieldBankAccountNO";
            this.pivotFieldBankAccountNO.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldBankAccountNO.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldBankAccountNO.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // PivotFieldAccountNameT
            // 
            this.PivotFieldAccountNameT.Appearance.Header.Options.UseTextOptions = true;
            this.PivotFieldAccountNameT.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.PivotFieldAccountNameT.AreaIndex = 7;
            resources.ApplyResources(this.PivotFieldAccountNameT, "PivotFieldAccountNameT");
            this.PivotFieldAccountNameT.Name = "PivotFieldAccountNameT";
            this.PivotFieldAccountNameT.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.PivotFieldAccountNameT.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.PivotFieldAccountNameT.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // pivotFieldAccountCodes
            // 
            this.pivotFieldAccountCodes.Appearance.Header.Options.UseTextOptions = true;
            this.pivotFieldAccountCodes.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotFieldAccountCodes.AreaIndex = 8;
            resources.ApplyResources(this.pivotFieldAccountCodes, "pivotFieldAccountCodes");
            this.pivotFieldAccountCodes.Name = "pivotFieldAccountCodes";
            this.pivotFieldAccountCodes.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldAccountCodes.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldAccountCodes.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldAccountCodes.Visible = false;
            // 
            // pivotFieldCurrencyNameT
            // 
            this.pivotFieldCurrencyNameT.Appearance.Header.Options.UseTextOptions = true;
            this.pivotFieldCurrencyNameT.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotFieldCurrencyNameT.AreaIndex = 8;
            resources.ApplyResources(this.pivotFieldCurrencyNameT, "pivotFieldCurrencyNameT");
            this.pivotFieldCurrencyNameT.Name = "pivotFieldCurrencyNameT";
            this.pivotFieldCurrencyNameT.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldCurrencyNameT.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldCurrencyNameT.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // pivotLastUpdateds
            // 
            this.pivotLastUpdateds.Appearance.Header.Options.UseTextOptions = true;
            this.pivotLastUpdateds.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotLastUpdateds.AreaIndex = 9;
            resources.ApplyResources(this.pivotLastUpdateds, "pivotLastUpdateds");
            this.pivotLastUpdateds.CellFormat.FormatString = "dd/MM/yyy";
            this.pivotLastUpdateds.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            this.pivotLastUpdateds.Name = "pivotLastUpdateds";
            this.pivotLastUpdateds.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.pivotLastUpdateds.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.pivotLastUpdateds.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            this.pivotLastUpdateds.ValueFormat.FormatString = "dd/MM/yyy";
            this.pivotLastUpdateds.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime;
            // 
            // pivotFieldStatusName
            // 
            this.pivotFieldStatusName.Appearance.Header.Options.UseTextOptions = true;
            this.pivotFieldStatusName.Appearance.Header.TextOptions.HAlignment = DevExpress.Utils.HorzAlignment.Center;
            this.pivotFieldStatusName.AreaIndex = 10;
            resources.ApplyResources(this.pivotFieldStatusName, "pivotFieldStatusName");
            this.pivotFieldStatusName.Name = "pivotFieldStatusName";
            this.pivotFieldStatusName.Options.AllowDrag = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldStatusName.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.pivotFieldStatusName.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // layoutControl1
            // 
            this.layoutControl1.Controls.Add(this.pivotGridBank);
            this.layoutControl1.Controls.Add(this.menuStrip1);
            resources.ApplyResources(this.layoutControl1, "layoutControl1");
            this.layoutControl1.Name = "layoutControl1";
            this.layoutControl1.Root = this.layoutControlGroup1;
            // 
            // menuStrip1
            // 
            resources.ApplyResources(this.menuStrip1, "menuStrip1");
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.MenuFetchData,
            this.MenuSave,
            this.MenuLoad,
            this.menuPreview});
            this.menuStrip1.Name = "menuStrip1";
            // 
            // MenuFetchData
            // 
            this.MenuFetchData.Name = "MenuFetchData";
            resources.ApplyResources(this.MenuFetchData, "MenuFetchData");
            this.MenuFetchData.Click += new System.EventHandler(this.toolMenuRefresh_Click);
            // 
            // MenuSave
            // 
            this.MenuSave.Name = "MenuSave";
            resources.ApplyResources(this.MenuSave, "MenuSave");
            this.MenuSave.Click += new System.EventHandler(this.toolMenuSave_Click);
            // 
            // MenuLoad
            // 
            this.MenuLoad.Name = "MenuLoad";
            resources.ApplyResources(this.MenuLoad, "MenuLoad");
            this.MenuLoad.Click += new System.EventHandler(this.toolMenuLoad_Click);
            // 
            // menuPreview
            // 
            this.menuPreview.Name = "menuPreview";
            resources.ApplyResources(this.menuPreview, "menuPreview");
            this.menuPreview.Click += new System.EventHandler(this.toolMenuPreview_Click);
            // 
            // layoutControlGroup1
            // 
            resources.ApplyResources(this.layoutControlGroup1, "layoutControlGroup1");
            this.layoutControlGroup1.EnableIndentsWithoutBorders = DevExpress.Utils.DefaultBoolean.True;
            this.layoutControlGroup1.GroupBordersVisible = false;
            this.layoutControlGroup1.Items.AddRange(new DevExpress.XtraLayout.BaseLayoutItem[] {
            this.layoutPivot,
            this.layoutMenuBank});
            this.layoutControlGroup1.Location = new System.Drawing.Point(0, 0);
            this.layoutControlGroup1.Name = "layoutControlGroup1";
            this.layoutControlGroup1.Padding = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.Size = new System.Drawing.Size(792, 566);
            this.layoutControlGroup1.Spacing = new DevExpress.XtraLayout.Utils.Padding(0, 0, 0, 0);
            this.layoutControlGroup1.TextVisible = false;
            // 
            // layoutPivot
            // 
            this.layoutPivot.Control = this.pivotGridBank;
            resources.ApplyResources(this.layoutPivot, "layoutPivot");
            this.layoutPivot.Location = new System.Drawing.Point(0, 25);
            this.layoutPivot.Name = "layoutPivot";
            this.layoutPivot.Size = new System.Drawing.Size(792, 541);
            this.layoutPivot.TextSize = new System.Drawing.Size(0, 0);
            this.layoutPivot.TextToControlDistance = 0;
            this.layoutPivot.TextVisible = false;
            // 
            // layoutMenuBank
            // 
            this.layoutMenuBank.Control = this.menuStrip1;
            resources.ApplyResources(this.layoutMenuBank, "layoutMenuBank");
            this.layoutMenuBank.Location = new System.Drawing.Point(0, 0);
            this.layoutMenuBank.Name = "layoutMenuBank";
            this.layoutMenuBank.Size = new System.Drawing.Size(792, 25);
            this.layoutMenuBank.TextSize = new System.Drawing.Size(0, 0);
            this.layoutMenuBank.TextToControlDistance = 0;
            this.layoutMenuBank.TextVisible = false;
            // 
            // ColUpdatedUser
            // 
            this.ColUpdatedUser.AreaIndex = 11;
            resources.ApplyResources(this.ColUpdatedUser, "ColUpdatedUser");
            this.ColUpdatedUser.Name = "ColUpdatedUser";
            this.ColUpdatedUser.Options.AllowFilter = DevExpress.Utils.DefaultBoolean.True;
            this.ColUpdatedUser.Options.AllowSort = DevExpress.Utils.DefaultBoolean.True;
            // 
            // FormBank
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Controls.Add(this.layoutControl1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "FormBank";
            center1.Company = "";
            center1.ErrorOn = true;
            center1.Password = null;
            center1.Server = null;
            center1.Username = null;
            this.QEBCenterInfo = center1;
            this.Load += new System.EventHandler(this.FormBank_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pivotGridBank)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControl1)).EndInit();
            this.layoutControl1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.layoutControlGroup1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutPivot)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.layoutMenuBank)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private ControlCenterAddition.XtraPivotGridControl pivotGridBank;
        private DevExpress.XtraPivotGrid.PivotGridField pivotFieldCode;
        private DevExpress.XtraPivotGrid.PivotGridField pivotFieldSwiftCode;
        private DevExpress.XtraPivotGrid.PivotGridField pivotFieldBankNameThai;
        private DevExpress.XtraPivotGrid.PivotGridField pivotFieldBankNameEng;
        private DevExpress.XtraPivotGrid.PivotGridField pivotFieldBankBranchThai;
        private DevExpress.XtraPivotGrid.PivotGridField pivotFieldBankBranchEng;
        private DevExpress.XtraPivotGrid.PivotGridField pivotFieldAddressThai;
        private DevExpress.XtraPivotGrid.PivotGridField pivotFieldAddressEng;
        private DevExpress.XtraPivotGrid.PivotGridField pivotFieldBankAccountType;
        private DevExpress.XtraPivotGrid.PivotGridField pivotFieldBankAccountNO;
        private DevExpress.XtraPivotGrid.PivotGridField PivotFieldAccountNameT;
        private DevExpress.XtraPivotGrid.PivotGridField pivotFieldAccountCodes;
        private DevExpress.XtraPivotGrid.PivotGridField pivotFieldCurrencyNameT;
        private DevExpress.XtraPivotGrid.PivotGridField pivotLastUpdateds;
        private DevExpress.XtraPivotGrid.PivotGridField pivotFieldStatusName;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private DevExpress.XtraLayout.LayoutControl layoutControl1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem MenuFetchData;
        private System.Windows.Forms.ToolStripMenuItem MenuSave;
        private System.Windows.Forms.ToolStripMenuItem MenuLoad;
        private System.Windows.Forms.ToolStripMenuItem menuPreview;
        private DevExpress.XtraLayout.LayoutControlGroup layoutControlGroup1;
        private DevExpress.XtraLayout.LayoutControlItem layoutPivot;
        private DevExpress.XtraLayout.LayoutControlItem layoutMenuBank;
        private DevExpress.XtraPivotGrid.PivotGridField ColUpdatedUser;
    }
}

